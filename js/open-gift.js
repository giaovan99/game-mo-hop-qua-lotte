import { confetti } from "https://cdn.jsdelivr.net/npm/tsparticles-confetti/+esm";
const warningText = document.querySelector(".warningText");
let titleChonQua = document.querySelector("#sectionChonQua .titleGift");

// Khai báo giải để so sánh và hiển thị hình + tên giải
const giai3 = "giai3";
const khongTrung = "khongTrung";

// Ảnh giải
const giai3Img =
  '<img src="./img/phan-thuong/giai-3.png" style="z-index:100" />';
const koTrungImg =
  '<img src="./img/phan-thuong/ko_trung.png" style="z-index:100" />';
// Tên giải
const imgTenGiai3Mobile =
  '<img src="./img/phan-thuong/ten-giai-trung.png" alt="" width="100%" style="scale:1.15; max-width:400px"/>';
const imgTenGiai3Desktop =
  '<img src="./img/phan-thuong/desktop/ten-giai-trung.png" alt="" width="100%" style="scale:1.15; max-width:400px"/>';
const imgTenKhongTrungMobile =
  '<img src="./img/phan-thuong/ten-giai-ko-trung.png" alt="" width="100%" style="scale:1.15; max-width:400px"/>';
const imgTenKhongTrungDesktop =
  '<img src="./img/phan-thuong/desktop/ten-giai-ko-trung.png" alt="" width="100%" style="scale:1.15; max-width:400px"/>';

// Pháo hoa
const run = () => {
  const confettiConfig = {
    spread: 250,
    ticks: 350,
    gravity: 1,
    decay: 0.94,
    startVelocity: 30,
    particleCount: 25,
    scalar: 10,
    shapes: ["image"],
    shapeOptions: {
      image: [
        {
          src: "./img/hoa-giay-icon/1.png",
          width: 160,
          height: 160,
        },
        {
          src: "./img/hoa-giay-icon/2.png",
          width: 160,
          height: 160,
        },
        {
          src: "./img/hoa-giay-icon/3.png",
          width: 160,
          height: 160,
        },
        {
          src: "./img/hoa-giay-icon/4.png",
          width: 160,
          height: 160,
        },
        {
          src: "./img/hoa-giay-icon/5.png",
          width: 160,
          height: 160,
        },
      ],
    },
    origin: { x: 0.2, y: 0.7 }, // Xác định vị trí bắt đầu của các hạt pháo giấy ở nửa bên phải
  };
  // Kiểm tra kích thước màn hình
  if (window.innerWidth > 768) {
    // Chỉ thêm thuộc tính origin khi màn hình lớn hơn 1024px
    confettiConfig.origin = { x: 0.8, y: 0.7 };
  }

  // Gọi hàm confetti với cấu hình
  confetti(confettiConfig);
  // confetti({
  //   spread: 250,
  //   ticks: 350,
  //   gravity: 1,
  //   decay: 0.94,
  //   startVelocity: 30,
  //   particleCount: 25,
  //   scalar: 10,
  //   shapes: ["image"],
  //   shapeOptions: {
  //     image: [
  //       {
  //         src: "./img/hoa-giay-icon/1.png",
  //         width: 160,
  //         height: 160,
  //       },
  //       {
  //         src: "./img/hoa-giay-icon/2.png",
  //         width: 160,
  //         height: 160,
  //       },
  //       {
  //         src: "./img/hoa-giay-icon/3.png",
  //         width: 160,
  //         height: 160,
  //       },
  //       {
  //         src: "./img/hoa-giay-icon/4.png",
  //         width: 160,
  //         height: 160,
  //       },
  //       {
  //         src: "./img/hoa-giay-icon/5.png",
  //         width: 160,
  //         height: 160,
  //       },
  //     ],
  //   },
  // });
};
// Ẩn 2 rương không được chọn
function hideChest(i, reward) {
  chestRewards.forEach((chestReward, index) => {
    if (index !== i) {
      // Ẩn trước
      chestReward.classList.toggle("hidden");
      // Biến mất sau 2s
      setTimeout(() => chestReward.remove(), 2000);
    } else {
      chestReward.querySelector(".lock-chest").classList.toggle("active");
      // Giải 3 Mobile & Desktop
      if (reward === giai3) {
        //  Gắn giải thưởng
        chestReward.querySelector(".name").innerHTML = giai3Img;
      } else {
        //  Gắn giải thưởng
        chestReward.querySelector(".name").innerHTML = koTrungImg;
      }
      // Đổi background (chỉ ở Mobile)
      if (screen.width < 1023) {
        document.querySelector(".img_layer").classList.toggle("hidden");
      }
      setTimeout(() => {
        // Bự từ từ mất 2s
        chestReward.classList.remove("col-4");
        chestReward.classList.add("active");
      }, 2000);
      // Sau khi zoom hộp quà
      setTimeout(() => {
        //Hiệu ứng mở hộp quà
        chestReward.querySelector(".chest-img").classList.toggle("shake-chest");
        chestReward.querySelector(".chest-img").classList.toggle("open-chest");
      }, 4000);
      setTimeout(() => {
        //Hiệu ứng bắn pháo hoa
        run();
        // Hiển thị title kết quả
        if (reward === giai3) {
          if (screen.width < 768) {
            titleChonQua.innerHTML = imgTenGiai3Mobile;
          } else {
            titleChonQua.innerHTML = imgTenGiai3Desktop;
          }
        } else {
          if (screen.width < 768) {
            titleChonQua.innerHTML = imgTenKhongTrungMobile;
          } else {
            titleChonQua.innerHTML = imgTenKhongTrungDesktop;
          }
        }
        document.querySelector(".renderQua").style.scale = "1.15";
        document.querySelector(".renderQua").style.margin = "0px 0 22px 0";
        let bgHaoQuang = document.createElement("img");
        bgHaoQuang.src = "./img/phan-thuong/hao-quang.png";
        bgHaoQuang.classList.add("haoQuangBg");
        document.querySelector(".renderQua").prepend(bgHaoQuang);
        // Scale giải thưởng lên
        chestReward.querySelector(".name").classList.toggle("active");
        // Hiển thị warningText
        warningText.classList.remove("hidden");
      }, 7000);
    }
  });
}

// ____________________ Sự kiện mở rương ___________________
const chestRewards = document.querySelectorAll(".chestReward");
chestRewards.forEach((chestReward, index) => {
  chestReward.addEventListener("click", () => {
    var audio = new Audio("./img/lottery-wheel/sound.mp3");
    // Hành động gọi lên BE lấy kết quả giải thưởng
    let reward = "khongTrung";

    // Khi đã có kết quả từ BE thì mới chạy
    if (reward) {
      audio.play();
      chestReward.style.pointerEvents = "none";
      hideChest(index, reward);
    }
  });
});
