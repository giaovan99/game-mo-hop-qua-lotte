let formLogin = document.querySelector("form#login"); //nút đăng nhập
const formItems = document.querySelectorAll("form#login input");
let modalTheLe = document.querySelector("#theleModal");
let confirmTheLe = false;

formItems.forEach((item, index) => {
  item.addEventListener("input", () => {
    if (item.value) {
      item.classList.remove("warning");
      item.parentElement.querySelector(".feedback").classList.add("hidden");
    } else {
      item.classList.add("warning");
      item.parentElement.querySelector(".feedback").classList.remove("hidden");
    }
  });
});

modalTheLe.addEventListener("shown.bs.modal", () => {
  modalTheLe.querySelector(".modal-body").addEventListener("scroll", () => {
    var scrollTop = modalTheLe.querySelector(".modal-body").scrollTop;
    var scrollHeight = modalTheLe.querySelector(".modal-body").scrollHeight; // added
    var offsetHeight = modalTheLe.querySelector(".modal-body").offsetHeight;
    var contentHeight = scrollHeight - offsetHeight; // added
    if (contentHeight - 2 <= scrollTop) {
      modalTheLe.querySelector(".modal-footer button").disabled = false;
    }
  });
});

modalTheLe
  .querySelector(".modal-footer button")
  .addEventListener("click", () => {
    confirmTheLe = true;
    formLogin.dispatchEvent(new Event("submit"));
  });

// --------------------------------------Xử lý đăng nhập--------------------------------------
formLogin &&
  formLogin.addEventListener("submit", (event) => {
    event.preventDefault();
    console.log("run");
    let data = {};
    let feedbacks = [];
    // Check rỗng các trường dữ liệu
    formItems.forEach((item, index) => {
      data[item.name] = item.value;
      // check rỗng
      if (!item.value) {
        item.classList.add("warning");
        item.parentElement
          .querySelector(".feedback")
          .classList.remove("hidden");
        // document.querySelector(".feedback.feedbackAPI").innerHTML =
        //   "Vui lòng nhập mã dự thưởng";
        feedbacks.push(item.name);
      } else {
        document.querySelector(".feedback.feedbackAPI").innerHTML = "";
      }
    });

    // Gọi API & check feedback API
    if (feedbacks.length == 0 || !feedbacks) {
      // Check đã đồng ý thể lệ chưa
      if (!confirmTheLe) {
        var modal = new bootstrap.Modal(modalTheLe);
        modal.show();
      } else {
        window.location.assign("/thong-tin.html");
      }
    }
  });
