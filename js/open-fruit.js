import { confetti } from "https://cdn.jsdelivr.net/npm/tsparticles-confetti/+esm";
const warningText = document.querySelector(".warningText");
let titleChonQua = document.querySelector("#sectionChonQua .titleGift");

// Khai báo giải để so sánh và hiển thị hình + tên giải
const giai3 = "giai3";
const khongTrung = "khongTrung";
let showGift = false;

// Ảnh giải
const giai3Img = "./img/phan-thuong/giai-3.png";
const koTrungImg =
  '<img src="./img/phan-thuong/ko_trung.png" style="z-index:100" />';
// Tên giải
const imgTenGiai3Mobile =
  '<img src="./img/phan-thuong/ten-giai-trung.png" alt="" width="100%" style="scale:1.15; max-width:400px"/>';
const imgTenGiai3Desktop =
  '<img src="./img/phan-thuong/desktop/ten-giai-trung.png" alt="" width="100%" style="scale:1.15; max-width:400px"/>';
const imgTenKhongTrungMobile =
  '<img src="./img/phan-thuong/ten-giai-ko-trung.png" alt="" width="100%" style="scale:1.15; max-width:400px"/>';
const imgTenKhongTrungDesktop =
  '<img src="./img/phan-thuong/desktop/ten-giai-ko-trung.png" alt="" width="100%" style="scale:1.15; max-width:400px"/>';

// Pháo hoa
// const run = () => {
//   const confettiConfig = {
//     spread: 500,
//     ticks: 250,
//     gravity: 1.3,
//     decay: 0.94,
//     startVelocity: 30,
//     particleCount: 40,
//     scalar: 3,
//     shapes: ["image"],
//     shapeOptions: {
//       image: [
//         {
//           src: "./img/trai-cay/icon/blueberry-1.svg",
//           width: 20,
//           height: 20,
//         },
//         {
//           src: "./img/trai-cay/icon/blueberry-2.svg",
//           width: 20,
//           height: 20,
//         },
//         {
//           src: "./img/trai-cay/icon/blueberry-3.svg",
//           width: 20,
//           height: 20,
//         },
//         {
//           src: "./img/trai-cay/icon/blueberry-4.svg",
//           width: 20,
//           height: 20,
//         },
//         {
//           src: "./img/trai-cay/icon/blueberry-5.svg",
//           width: 20,
//           height: 20,
//         },
//         {
//           src: "./img/trai-cay/icon/leaf.svg",
//           width: 20,
//           height: 20,
//         },
//         {
//           src: "./img/trai-cay/icon/leaf-2.svg",
//           width: 20,
//           height: 20,
//         },
//       ],
//     },
//     origin: { x: 0.5, y: 0.5 }, // Xác định vị trí bắt đầu của các hạt pháo giấy ở nửa bên phải
//   };
//   // Kiểm tra kích thước màn hình
//   if (window.innerWidth > 768) {
//     // Chỉ thêm thuộc tính origin khi màn hình lớn hơn 1024px
//     confettiConfig.origin = { x: 0.5, y: 0.5 };
//   }

//   // Gọi hàm confetti với cấu hình
//   confetti(confettiConfig);
// };

// // Hàm chém trái cây
// function drawSlashOnCanvas(canvas) {
//   const ctx = canvas.getContext("2d");

//   const startX = 0;
//   const startY = canvas.height;
//   const endX = canvas.width;
//   const endY = 0;

//   const flashDuration = 100; // Thời gian của hiệu ứng flash (milliseconds)
//   const flashInterval = 10; // Khoảng thời gian giữa các bước trong hiệu ứng flash (milliseconds)
//   const flashSteps = flashDuration / flashInterval; // Số bước trong hiệu ứng flash
//   const flashLineWidth = 2; // Độ rộng của đường chém trong hiệu ứng flash

//   function drawFlashEffect() {
//     let currentLineWidth = 0;

//     // Vẽ từng bước của hiệu ứng flash
//     const flashIntervalId = setInterval(() => {
//       ctx.clearRect(0, 0, canvas.width, canvas.height);

//       // Tạo gradient từ trắng tới trong suốt
//       const gradient = ctx.createLinearGradient(startX, startY, endX, endY);
//       gradient.addColorStop(0, "rgba(255, 255, 255, 1)");
//       gradient.addColorStop(0.5, "rgba(255, 255, 255, 0.8)");
//       gradient.addColorStop(1, "rgba(255, 255, 255, 0.5)");

//       // Vẽ đường chém
//       ctx.beginPath();
//       ctx.moveTo(startX, startY);
//       ctx.lineTo(endX, endY);
//       ctx.strokeStyle = gradient; // Sử dụng gradient cho màu sắc của đường chém
//       ctx.lineCap = "round"; // Đầu đuôi của đường chém là tròn
//       ctx.lineWidth = currentLineWidth;
//       ctx.stroke();
//       ctx.closePath();

//       // Tăng độ rộng của đường chém cho đến khi đạt độ rộng cần thiết
//       currentLineWidth += flashLineWidth / flashSteps;

//       // Kết thúc hiệu ứng khi đạt độ rộng tối đa
//       if (currentLineWidth >= flashLineWidth) {
//         clearInterval(flashIntervalId);
//         setTimeout(() => {
//           ctx.clearRect(0, 0, canvas.width, canvas.height);
//         }, 200); // Xóa canvas sau khi hoàn thành hiệu ứng flash
//       }
//     }, flashInterval);
//   }

//   // Gọi hàm vẽ hiệu ứng flash
//   drawFlashEffect();
// }

// Ẩn 2 phần quà không được chọn & hiệu ứng chém trái cây
function hideGift(i, reward) {
  rewards.forEach((fruit, index) => {
    fruit.style.pointerEvents = "none";
    // TH: 2 phần thưởng không được chọn
    if (index !== i) {
      // Ẩn trước
      fruit.classList.toggle("hidden");
    }
    // TH: phần thưởng đc chọn
    else {
      fruit.classList.toggle("active");
      // Tạo ra thẻ canvas để làm đường cắt trái cây
      const canvas = document.createElement("canvas");
      canvas.id = "canvas";
      fruit.querySelector(".gift-wrapper").appendChild(canvas);
      console.log(fruit);
      fruit
        .querySelector(".reward-wrapper .reward img")
        .setAttribute("src", giai3Img);

      setTimeout(() => {
        fruit.querySelector(".gift-wrapper").classList.toggle("open");
      }, 3000);
      setTimeout(() => {
        fruit.querySelector(".reward-wrapper").classList.toggle("active");
        //Hiệu ứng bắn pháo hoa
        // run();
      }, 5500);
    }
  });
}

// ____________________ Sự kiện chọn phần thưởng ___________________
const rewards = document.querySelectorAll(".fruit-item");
rewards.forEach((gift, index) => {
  gift.addEventListener("click", () => {
    var audio = new Audio("./img/trai-cay/sound/fluffing-a-duck.mp3");
    // Hành động gọi lên BE lấy kết quả giải thưởng
    let reward = "khongTrung";
    // Khi đã có kết quả từ BE thì mới chạy
    if (reward) {
      audio.play();
      hideGift(index, reward);
    }
  });
});
