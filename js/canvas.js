/**
 * Bundled by jsDelivr using Rollup v2.79.1 and Terser v5.19.2.
 * Original file: /npm/tsparticles-confetti@2.12.0/esm/index.js
 *
 * Do NOT use SRI with dynamically generated files! More information: https://www.jsdelivr.com/using-sri-with-dynamic-files
 */
import {
  isArray as t,
  deepExtend as i,
  isString as e,
  tsParticles as s,
  isSsr as a,
} from "/npm/tsparticles-engine@2.12.0/+esm";
import { loadBasic as o } from "/npm/tsparticles-basic@2.12.0/+esm";
import { loadCardsShape as n } from "/npm/tsparticles-shape-cards@2.12.0/+esm";
import { loadEmittersPlugin as r } from "/npm/tsparticles-plugin-emitters@2.12.0/+esm";
import { loadHeartShape as p } from "/npm/tsparticles-shape-heart@2.12.0/+esm";
import { loadImageShape as c } from "/npm/tsparticles-shape-image@2.12.0/+esm";
import { loadLifeUpdater as d } from "/npm/tsparticles-updater-life@2.12.0/+esm";
import { loadMotionPlugin as l } from "/npm/tsparticles-plugin-motion@2.12.0/+esm";
import { loadPolygonShape as m } from "/npm/tsparticles-shape-polygon@2.12.0/+esm";
import { loadRollUpdater as h } from "/npm/tsparticles-updater-roll@2.12.0/+esm";
import { loadRotateUpdater as f } from "/npm/tsparticles-updater-rotate@2.12.0/+esm";
import { loadSquareShape as u } from "/npm/tsparticles-shape-square@2.12.0/+esm";
import { loadStarShape as y } from "/npm/tsparticles-shape-star@2.12.0/+esm";
import { loadTextShape as v } from "/npm/tsparticles-shape-text@2.12.0/+esm";
import { loadTiltUpdater as g } from "/npm/tsparticles-updater-tilt@2.12.0/+esm";
import { loadWobbleUpdater as x } from "/npm/tsparticles-updater-wobble@2.12.0/+esm";
class b {
  constructor() {
    (this.angle = 90),
      (this.count = 50),
      (this.spread = 45),
      (this.startVelocity = 45),
      (this.decay = 0.9),
      (this.gravity = 1),
      (this.drift = 0),
      (this.ticks = 200),
      (this.position = { x: 50, y: 50 }),
      (this.colors = [
        "#26ccff",
        "#a25afd",
        "#ff5e7e",
        "#88ff5a",
        "#fcff42",
        "#ffa62d",
        "#ff36ff",
      ]),
      (this.shapes = ["square", "circle"]),
      (this.scalar = 1),
      (this.zIndex = 100),
      (this.disableForReducedMotion = !0),
      (this.shapeOptions = {});
  }
  get origin() {
    return { x: this.position.x / 100, y: this.position.y / 100 };
  }
  set origin(t) {
    (this.position.x = 100 * t.x), (this.position.y = 100 * t.y);
  }
  get particleCount() {
    return this.count;
  }
  set particleCount(t) {
    this.count = t;
  }
  load(e) {
    if (!e) return;
    void 0 !== e.angle && (this.angle = e.angle);
    const s = e.count ?? e.particleCount;
    void 0 !== s && (this.count = s),
      void 0 !== e.spread && (this.spread = e.spread),
      void 0 !== e.startVelocity && (this.startVelocity = e.startVelocity),
      void 0 !== e.decay && (this.decay = e.decay),
      void 0 !== e.gravity && (this.gravity = e.gravity),
      void 0 !== e.drift && (this.drift = e.drift),
      void 0 !== e.ticks && (this.ticks = e.ticks);
    const a = e.origin;
    a &&
      !e.position &&
      (e.position = {
        x: void 0 !== a.x ? 100 * a.x : void 0,
        y: void 0 !== a.y ? 100 * a.y : void 0,
      });
    const o = e.position;
    o &&
      (void 0 !== o.x && (this.position.x = o.x),
      void 0 !== o.y && (this.position.y = o.y)),
      void 0 !== e.colors &&
        (t(e.colors)
          ? (this.colors = [...e.colors])
          : (this.colors = e.colors));
    const n = e.shapeOptions;
    if (void 0 !== n)
      for (const t in n) {
        const e = n[t];
        e && (this.shapeOptions[t] = i(this.shapeOptions[t] ?? {}, e));
      }
    void 0 !== e.shapes &&
      (t(e.shapes) ? (this.shapes = [...e.shapes]) : (this.shapes = e.shapes)),
      void 0 !== e.scalar && (this.scalar = e.scalar),
      void 0 !== e.zIndex && (this.zIndex = e.zIndex),
      void 0 !== e.disableForReducedMotion &&
        (this.disableForReducedMotion = e.disableForReducedMotion);
  }
}
let w = !1,
  z = !1;
const I = new Map();
async function V(t) {
  if (!w) {
    if (z)
      return new Promise((t) => {
        const i = setInterval(() => {
          w && (clearInterval(i), t());
        }, 100);
      });
    (z = !0),
      await o(t),
      await r(t),
      await l(t),
      await n(t),
      await p(t),
      await c(t),
      await m(t),
      await u(t),
      await y(t),
      await v(t),
      await f(t),
      await d(t),
      await h(t),
      await g(t),
      await x(t),
      (z = !1),
      (w = !0);
  }
}
async function k(t) {
  const i = new b();
  let e;
  i.load(t.options);
  const a = (1e3 * i.ticks) / 432e3;
  if (I.has(t.id) && ((e = I.get(t.id)), e && !e.destroyed)) {
    const t = e;
    if (t.addEmitter)
      return void t.addEmitter({
        startCount: i.count,
        position: i.position,
        size: { width: 0, height: 0 },
        rate: { delay: 0, quantity: 0 },
        life: { duration: 0.1, count: 1 },
        particles: {
          color: { value: i.colors },
          shape: { type: i.shapes, options: i.shapeOptions },
          life: { count: 1 },
          opacity: {
            value: { min: 0, max: 1 },
            animation: {
              enable: !0,
              sync: !0,
              speed: a,
              startValue: "max",
              destroy: "min",
            },
          },
          size: { value: 5 * i.scalar },
          move: {
            angle: { value: i.spread, offset: 0 },
            drift: { min: -i.drift, max: i.drift },
            gravity: { acceleration: 9.81 * i.gravity },
            speed: 3 * i.startVelocity,
            decay: 1 - i.decay,
            direction: -i.angle,
          },
        },
      });
  }
  const o = {
    fullScreen: { enable: !t.canvas, zIndex: i.zIndex },
    fpsLimit: 120,
    particles: {
      number: { value: 0 },
      color: { value: i.colors },
      shape: { type: i.shapes, options: i.shapeOptions },
      opacity: {
        value: { min: 0, max: 1 },
        animation: {
          enable: !0,
          sync: !0,
          speed: a,
          startValue: "max",
          destroy: "min",
        },
      },
      size: { value: 5 * i.scalar },
      links: { enable: !1 },
      life: { count: 1 },
      move: {
        angle: { value: i.spread, offset: 0 },
        drift: { min: -i.drift, max: i.drift },
        enable: !0,
        gravity: { enable: !0, acceleration: 9.81 * i.gravity },
        speed: 3 * i.startVelocity,
        decay: 1 - i.decay,
        direction: -i.angle,
        random: !0,
        straight: !1,
        outModes: { default: "none", bottom: "destroy" },
      },
      rotate: {
        value: { min: 0, max: 360 },
        direction: "random",
        animation: { enable: !0, speed: 60 },
      },
      tilt: {
        direction: "random",
        enable: !0,
        value: { min: 0, max: 360 },
        animation: { enable: !0, speed: 60 },
      },
      roll: {
        darken: { enable: !0, value: 25 },
        enable: !0,
        speed: { min: 15, max: 25 },
      },
      wobble: { distance: 30, enable: !0, speed: { min: -15, max: 15 } },
    },
    detectRetina: !0,
    motion: { disable: i.disableForReducedMotion },
    emitters: {
      name: "confetti",
      startCount: i.count,
      position: i.position,
      size: { width: 0, height: 0 },
      rate: { delay: 0, quantity: 0 },
      life: { duration: 0.1, count: 1 },
    },
  };
  return (
    (e = await s.load({ id: t.id, element: t.canvas, options: o })),
    I.set(t.id, e),
    e
  );
}
async function M(t, i) {
  let a, o;
  return (
    await V(s),
    e(t) ? ((o = t), (a = i ?? {})) : ((o = "confetti"), (a = t)),
    k({ id: o, options: a })
  );
}
(M.create = async (t, i) => {
  if (!t) return M;
  await V(s);
  const a = t.getAttribute("id") || "confetti";
  return (
    t.setAttribute("id", a),
    async (s, o) => {
      let n, r;
      return (
        e(s) ? ((r = s), (n = o ?? i)) : ((r = a), (n = s)),
        k({ id: r, canvas: t, options: n })
      );
    }
  );
}),
  (M.version = s.version),
  a() || (window.confetti = M);
export { M as confetti };
export default null;
//# sourceMappingURL=/sm/b58f24419e36332dc9396d084933eb588f66135d7f6efed0a6ec9826aacc7e01.map
