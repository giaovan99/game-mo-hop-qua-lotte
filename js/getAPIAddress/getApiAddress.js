import { api } from "../../apis/base.js";

export const getApiAddress = {
  getProvince: async () => {
    try {
      return await api.get("/province");
    } catch (error) {
      alert("Đã có lỗi xảy ra");
      console.log(error);
      return false;
    }
  },
  getDistrict: async (data) => {
    try {
      return await api.get(`/district?province_id=${data}`);
    } catch (error) {
      alert("Đã có lỗi xảy ra");
      console.log(error);
      return false;
    }
  },
  getWard: async (data) => {
    try {
      return await api.get(`/ward?district_id=${data}`);
    } catch (error) {
      alert("Đã có lỗi xảy ra");
      console.log(error);
      return false;
    }
  },
};
