import { getApiAddress } from "./getAPIAddress/getApiAddress.js";

let formThongTin = document.querySelector("form#thongTinCaNhan"); //Form thông tin
let proviceName = document.querySelector("[name=province]"); //Thẻ select tỉnh tp
let districtName = document.querySelector("[name=district]"); //Thẻ select quận huyện
let wardName = document.querySelector("[name=ward]"); //Thẻ select quận huyện
let checkSubmit = 0;
const formItems = document.querySelectorAll(
  "form#thongTinCaNhan input, form#thongTinCaNhan select"
);

function checkPhone(value) {
  const regexPhoneNumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
  return value.match(regexPhoneNumber) ? true : false;
}

formItems.forEach((item, index) => {
  item.addEventListener("input", () => {
    console.log(item.value);
    if (item.value && item.name != "phone") {
      item.classList.remove("warning");
      item.parentElement.querySelector(".feedback").classList.add("hidden");
    } else if (item.value && item.name == "phone") {
      if (checkPhone(item.value)) {
        item.classList.remove("warning");
        item.parentElement.querySelector(".feedback").classList.add("hidden");
      } else {
        item.classList.add("warning");
        item.parentElement
          .querySelector(".feedback")
          .classList.remove("hidden");
      }
    } else {
      item.classList.add("warning");
      item.parentElement.querySelector(".feedback").classList.remove("hidden");
    }
  });
});

formThongTin &&
  formThongTin.addEventListener("submit", (event) => {
    checkSubmit++;
    event.preventDefault();
    let data = {};
    let feedbacks = [];
    formItems.forEach((item, index) => {
      data[item.name] = item.value;
      // check rỗng
      if (!item.value) {
        item.classList.add("warning");
        item.parentElement
          .querySelector(".feedback")
          .classList.remove("hidden");
        feedbacks.push(item.name);
      }
      // Check trường hợp phone
      else if (item.value && item.name == "phone") {
        if (checkPhone(item.value)) {
          item.classList.remove("warning");
          item.parentElement.querySelector(".feedback").classList.add("hidden");
        } else {
          item.classList.add("warning");
          item.parentElement
            .querySelector(".feedback")
            .classList.remove("hidden");
          feedbacks.push(item.name);
        }
      }
    });
    if (feedbacks.length == 0 || !feedbacks) {
      window.location.assign("/ket-qua.html");
    }
  });

// Gọi API để trả về tỉnh TP
const callAPIAdress = async () => {
  // Lấy thông tin tỉnh
  let res = await getApiAddress.getProvince();
  if (res.data.code == 200) {
    let provinceList = res.data.data; //Danh sách các tỉnh API trả về
    let content = ""; // Chứa nội dung html
    for (const province of provinceList) {
      content += `<option value=${province.ProvinceID} class='color_black'>${province.ProvinceName}</option>`;
    }
    content =
      `<option value="" selected disabled class='color_gray'>Tỉnh/Thành phố</option>` +
      content;
    proviceName.innerHTML = content;
  }
};

// Gọi API trả về quận huyện tương ứng của tỉnh/tp đã chọn
const callAPIDistrict = async (id) => {
  let res = await getApiAddress.getDistrict(id);
  if (res.data.code == 200) {
    let districtList = res.data.data; //Danh sách các quận huyện API trả về
    let content = ""; // Chứa nội dung html
    for (const district of districtList) {
      content += `<option value=${district.DistrictID} class='color_black'>${district.DistrictName}</option>`;
    }
    content =
      `<option value="" selected disabled class='color_gray'>Quận/Huyện</option>` +
      content;
    districtName.innerHTML = content;
  }
};

// Gọi API trả về phường xã tương ứng của quận/huyện đã chọn
// const callAPIWard = async (id) => {
//   let res = await getApiAddress.getWard(id);
//   if (res.data.code == 200) {
//     let wardList = res.data.data; //Danh sách các phường xã API trả về
//     let content = ""; // Chứa nội dung html
//     for (const ward of wardList) {
//       content += `<option value=${ward.WardCode}>${ward.WardName}</option>`;
//     }
//     content =
//       `<option value="" selected disabled >Chọn Phường/Xã</option>` + content;
//     wardName.innerHTML = content;
//   }
// };

proviceName.addEventListener("change", function (e) {
  if (e.target.value) {
    districtName.removeAttribute("disabled");
    callAPIDistrict(e.target.value);
  }
});

// districtName.addEventListener("change", function (e) {
//   if (e.target.value) {
//     wardName.removeAttribute("disabled");
//     // callAPIWard(e.target.value);
//   }
// });

callAPIAdress();

var myselect = document.getElementById("select1");
var myselect2 = document.getElementById("select2");

export function colourFunction() {
  var colour = myselect.options[myselect.selectedIndex].className;
  var colour2 = myselect2.options[myselect2.selectedIndex].className;
  myselect.className = colour;
  myselect2.className = colour2;
}

colourFunction();
// Attach event listener
document.addEventListener("DOMContentLoaded", () => {
  document.getElementById("select1").addEventListener("change", colourFunction);
  document.getElementById("select2").addEventListener("change", colourFunction);
});
