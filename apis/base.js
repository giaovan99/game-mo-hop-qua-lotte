const BASE_URL = "https://online-gateway.ghn.vn/shiip/public-api/master-data";
const TOKEN_URL = "1d305930-dabf-11ee-8586-12380ed2f541";

export const api = axios.create({
  baseURL: BASE_URL,
  headers: { ContentType: "application/json", Token: TOKEN_URL },
  timeout: 30000,
});
